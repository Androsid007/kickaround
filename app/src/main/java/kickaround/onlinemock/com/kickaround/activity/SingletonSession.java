package kickaround.onlinemock.com.kickaround.activity;

import android.app.ProgressDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import kickaround.onlinemock.com.kickaround.adapter.ChannelAdapter;
import kickaround.onlinemock.com.kickaround.model.Channel;
import kickaround.onlinemock.com.kickaround.model.Doubt;

public class SingletonSession {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private RecyclerView channelRecyclerView;
    private ChannelAdapter channelAdapter;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference db;
    private FirebaseHelper helper;
    private ArrayList<Channel> channel = new ArrayList<Channel>();
    private ArrayList<Doubt> doubtList = new ArrayList<Doubt>();
    private Toolbar toolbar;
    ProgressDialog progressDialog;
    Boolean saved=null;

    private static SingletonSession instance;
    private String username;

    boolean isForSelectedChennel = false;

    //no outer class can initialize this class's object
    private SingletonSession() {

        db= FirebaseDatabase.getInstance().getReference();
        helper=new FirebaseHelper(db);
        retrieve1();
       // channel = retrieve(true);

        // String name = nameEditTxt.getText().toString();



      }

    public static SingletonSession Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new SingletonSession();
        }
        return instance;
    }

    public ArrayList<Channel> retrieve(boolean isForSelectedChennel) {

        this.isForSelectedChennel  = isForSelectedChennel;

         ArrayList<Channel> channel1 = new ArrayList<Channel>();



        for (Channel ds : channel)
        {
            //String name=ds.getValue(Channel.class).getName();
            Channel chan = (Channel) ds;

            if ( isForSelectedChennel )
            {
                if ( chan.isChecked())
                    channel1.add(chan);

            }
            else {
               channel1.add(chan);

            }
        }
        return  channel1;
    }




    public ArrayList<Channel> retrieve1()
    {


        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return channel;
    }
    public Boolean save(Channel spacecraft,int pos)
    {
        if(spacecraft==null)
        {
            saved=false;
        }else
        {
            try
            {
               // db.child("Channel").push().setValue(spacecraft);
              // db.child("Channel").child("checked").setValue(spacecraft.isChecked());
                db.child("Channel").child(String.valueOf(pos)).child("checked").setValue(spacecraft.isChecked());



                saved=true;

            }catch (DatabaseException e)
            {
                e.printStackTrace();
                saved=false;
            }
        }

        return saved;
    }

    private void fetchData(DataSnapshot dataSnapshot)
    {
        channel.clear();

        for (DataSnapshot ds : dataSnapshot.getChildren())
        {
            //String name=ds.getValue(Channel.class).getName();
            Channel chan = (Channel) ds.getValue(Channel.class);


//            if ( isForSelectedChennel )
//            {
//                if ( chan.getChecked())
//                     channel.add(chan);
//
//            }
//            else {
                channel.add(chan);

//            }
        }



    }






  /*  public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
*/}
