package kickaround.onlinemock.com.kickaround.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kickaround.onlinemock.com.kickaround.R;
import kickaround.onlinemock.com.kickaround.activity.DoubtActivity;
import kickaround.onlinemock.com.kickaround.model.Channel;

/**
 * Created by hp on 4/10/2017.
 */

public class SuscriberAdapter extends RecyclerView.Adapter<SuscriberAdapter.MyViewHolder> {

    private ArrayList<Channel> moviesList;
    private Context context;
    private int pos;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,number,doubtsNumber;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.channel_txt);
            number = (TextView) view.findViewById(R.id.channel_number_txt);
            cardView = (CardView)view.findViewById(R.id.subscribe_card);
            doubtsNumber = (TextView)view.findViewById(R.id.doubtsTxt);

        }
    }


    public SuscriberAdapter(ArrayList<Channel> moviesList,Context mContext) {
        this.moviesList = moviesList;
        this.context  = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.suscribe_channel, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        pos=position;
         Channel channel = moviesList.get(position);
       String noOfDoubts = String.valueOf(channel.getDoubts().size());
        holder.title.setText(moviesList.get(position).getName());
        holder.number.setText(moviesList.get(position).getChannelNumber());
        holder.doubtsNumber.setText(noOfDoubts);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, DoubtActivity.class);
                intent.putExtra("chnlName",moviesList.get(pos).getName());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
