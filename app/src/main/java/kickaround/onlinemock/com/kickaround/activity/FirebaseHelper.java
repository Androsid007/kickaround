package kickaround.onlinemock.com.kickaround.activity;

/**
 * Created by hp1 on 4/8/2017.
 */

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

import kickaround.onlinemock.com.kickaround.model.Channel;
import kickaround.onlinemock.com.kickaround.model.Doubt;

/**
 * Created by Oclemy on 6/21/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class FirebaseHelper {

    DatabaseReference db;
    Boolean saved=null;
    ArrayList<String> space=new ArrayList<>();
    Channel channels = new Channel();

    public FirebaseHelper(DatabaseReference db) {
        this.db = db;
    }

    //WRITE
    public Boolean save(Channel channel)
    {
        if(channel ==null)
        {
            saved=false;
        }else
        {
            try
            {
                db.child("Channel").push().setValue(channel);
                saved=true;

            }catch (DatabaseException e)
            {
                e.printStackTrace();
                saved=false;
            }
        }

        return saved;
    }

    //READ
    public ArrayList<String> retrieve()
    {
        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return space;
    }

    private void fetchData(DataSnapshot dataSnapshot)
    {
        //spacecrafts.clear();

        for (DataSnapshot ds : dataSnapshot.getChildren())
        {
            channels = ds.getValue(Channel.class);
            String name=ds.getValue(Channel.class).getName();

            space.add(name);

            String channel = ds.getValue(Channel.class).getChannelNumber();
            space.add(channel);

            List<Doubt> doubtList = channels.getDoubts();
            space.add(String.valueOf(doubtList));


        }
    }
}
