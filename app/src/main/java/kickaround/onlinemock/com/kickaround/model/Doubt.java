package kickaround.onlinemock.com.kickaround.model;

import java.util.List;

/**
 * Created by hp on 4/23/2017.
 */

public class Doubt {


    private String Doubt;

    private List<Posts> posts = null;

    public String getDoubt() {
        return Doubt;
    }

    public void setDoubt(String doubt) {
        this.Doubt = doubt;
    }

    public List<Posts> getPosts() {
        return posts;
    }

    public void setPosts(List<Posts> posts) {
        this.posts = posts;
    }

}
