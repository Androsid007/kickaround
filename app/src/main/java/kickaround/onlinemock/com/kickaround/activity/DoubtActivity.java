package kickaround.onlinemock.com.kickaround.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import kickaround.onlinemock.com.kickaround.R;
import kickaround.onlinemock.com.kickaround.adapter.DoubtsAdapter;
import kickaround.onlinemock.com.kickaround.model.Channel;
import kickaround.onlinemock.com.kickaround.model.Doubt;

public class DoubtActivity extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private RecyclerView channelRecyclerView;
    private DoubtsAdapter channelAdapter;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference db;
    private FirebaseHelper helper;
    private ArrayList<Channel> channel = new ArrayList<Channel>();
    private List<Doubt> doubts = new ArrayList<>();
    private Toolbar toolbar;
    ProgressDialog progressDialog;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doubt);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Doubts");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);

        name = getIntent().getExtras().getString("chnlName");


        auth = FirebaseAuth.getInstance();



        db= FirebaseDatabase.getInstance().getReference();
        helper=new FirebaseHelper(db);
        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(DoubtActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        channelRecyclerView = (RecyclerView) findViewById(R.id.doubts_recyclerview);

        channel = SingletonSession.Instance().retrieve(true);

        for (int i=0;i<channel.size();i++){
            String cName = channel.get(i).getName();
            if (cName.equals(name)){
                doubts = channel.get(0).getDoubts();
            }
        }

        showChannel();
    }

    public void showChannel(){
        channelRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        channelRecyclerView.setLayoutManager(mLayoutManager);
        channelRecyclerView.setItemAnimator(new DefaultItemAnimator());
        channelAdapter = new DoubtsAdapter(doubts,this);
        channelRecyclerView.setAdapter(channelAdapter);
    }
}
