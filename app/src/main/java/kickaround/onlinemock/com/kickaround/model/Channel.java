package kickaround.onlinemock.com.kickaround.model;

/**
 * Created by hp1 on 4/8/2017.
 */

import java.util.List;

/**
 * Created by Oclemy on 6/21/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class Channel {


    private String name;

    private String channelNumber;

    private List<Doubt> Doubts = null;



    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(String channelNumber) {
        this.channelNumber = channelNumber;
    }

    public List<Doubt> getDoubts() {
        return Doubts;
    }

    public void setDoubts(List<Doubt> doubts) {
        this.Doubts = doubts;
    }

}