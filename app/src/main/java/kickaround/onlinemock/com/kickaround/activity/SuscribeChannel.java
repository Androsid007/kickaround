package kickaround.onlinemock.com.kickaround.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import kickaround.onlinemock.com.kickaround.R;
import kickaround.onlinemock.com.kickaround.adapter.SuscriberAdapter;
import kickaround.onlinemock.com.kickaround.model.Channel;

/**
 * Created by hp1 on 4/21/2017.
 */

public class SuscribeChannel extends AppCompatActivity {
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private RecyclerView channelRecyclerView;
    private SuscriberAdapter channelAdapter;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference db;
    private FirebaseHelper helper;
    private ArrayList<Channel> channel = new ArrayList<Channel>();
    private Toolbar toolbar;
    ProgressDialog progressDialog;
    private final int interval = 10000; // 1 Second
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Channels");
        supportInvalidateOptionsMenu();
        toolbar.setTitleTextColor(Color.WHITE);
        showProgressbar();

        //get firebase auth instance
        auth = FirebaseAuth.getInstance();



        db= FirebaseDatabase.getInstance().getReference();
        helper=new FirebaseHelper(db);
        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(SuscribeChannel.this, LoginActivity.class));
                    finish();
                }
            }
        };


        channelRecyclerView = (RecyclerView) findViewById(R.id.lv);

        SingletonSession.Instance();


        Runnable runnable = new Runnable(){
            public void run() {

                channel = SingletonSession.Instance().retrieve(true);

                if(channel.size()>0) {
                    // stopProgressbar();

                    channelRecyclerView.setHasFixedSize(true);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    channelRecyclerView.setLayoutManager(mLayoutManager);
                    //recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                    channelRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    channelAdapter = new SuscriberAdapter(channel,SuscribeChannel.this);
                    channelRecyclerView.setAdapter(channelAdapter);
                    stopProgressbar();
                }
                else{

                    stopProgressbar();

                 //Toast.makeText(SuscribeChannel.this, "No suscrive found", Toast.LENGTH_SHORT).show();

       Intent intent = new Intent(SuscribeChannel.this,SuscribeChannel.class);
                    SuscribeChannel.this.startActivity(intent);

                }

            }
        };

        handler.postAtTime(runnable, System.currentTimeMillis()+interval);
        handler.postDelayed(runnable, interval);


    }

    //sign out method
    public void signOut() {
        auth.signOut();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }




    public void showProgressbar() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgressbar() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.select).setEnabled(true);

        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();


        if (id == R.id.select) {

            Intent intent = new Intent(this,MainActivity.class);
            this.startActivity(intent);
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

}
