package kickaround.onlinemock.com.kickaround.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import kickaround.onlinemock.com.kickaround.R;
import kickaround.onlinemock.com.kickaround.model.Doubt;

/**
 * Created by hp on 4/10/2017.
 */

public class DoubtsAdapter extends RecyclerView.Adapter<DoubtsAdapter.MyViewHolder> {

    private List<Doubt> doubtArrayList;
    private Context context;
    private int pos;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,number,doubtsNumber;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.channel_txt);
        /*    number = (TextView) view.findViewById(R.id.channel_number_txt);
            cardView = (CardView)view.findViewById(R.id.subscribe_card);
            doubtsNumber = (TextView)view.findViewById(R.id.doubtsTxt);
*/
        }
    }


    public DoubtsAdapter(List<Doubt> doubtList, Context mContext) {
        this.doubtArrayList = doubtList;
        this.context  = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.suscribe_channel, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        pos = position;
        final Doubt doubt = doubtArrayList.get(0);
        //String noOfDoubts = String.valueOf(channel.getDoubts().size());
        holder.title.setText(doubt.getDoubt());

    }

    @Override
    public int getItemCount() {
        return doubtArrayList.size();
    }
}
