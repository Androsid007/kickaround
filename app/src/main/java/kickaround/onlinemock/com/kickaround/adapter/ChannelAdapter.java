package kickaround.onlinemock.com.kickaround.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import kickaround.onlinemock.com.kickaround.R;
import kickaround.onlinemock.com.kickaround.activity.SingletonSession;
import kickaround.onlinemock.com.kickaround.model.Channel;

/**
 * Created by hp on 4/10/2017.
 */

public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.MyViewHolder> {

    private ArrayList<Channel> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,number;
        private Switch swith;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.channel_txt);
            number = (TextView) view.findViewById(R.id.channel_number_txt);
            swith=(Switch)view.findViewById(R.id.switch1);

             int i = getAdapterPosition();


        }
    }


    public ChannelAdapter(ArrayList<Channel> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_channel, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Channel movie = moviesList.get(position);
        holder.title.setText(moviesList.get(position).getName());
        holder.number.setText(moviesList.get(position).getChannelNumber());
        holder.swith.setChecked(moviesList.get(position).isChecked());
        holder.swith.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (((Switch) view).isChecked()) {
                    //Case 1
                    moviesList.get(position).setChecked(true);

                    Channel channel  = moviesList.get(position);
                    channel.setChecked(true);
                    SingletonSession.Instance().save(channel,position);
                }
                else {
                    moviesList.get(position).setChecked(false);
                    //case 2

                    Channel channel  = moviesList.get(position);
                    channel.setChecked(false);
                    SingletonSession.Instance().save(channel,position);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
